ECHO This will immediately close many programs without additional warning
ECHO Be sure to SAVE YOUR WORK before proceding
ECHO If you do not wish to proceed, close this window.
ECHO Press any key to continue
pause
taskkill /im chrome* /f /t
taskkill /im firefox* /f /t
taskkill /im word* /f /t
taskkill /im adobe* /f /t
taskkill /im excel* /f /t
taskkill /im notepad* /f /t
taskkill /im nv* /f /t
taskkill /im RAVB* /f /t
taskkill /im rtk* /f /t
taskkill /im searchui.exe /f /t
taskkill /im google* /f /t
taskkill /im asus* /f /t
taskkill /im intel* /f /t
taskkill /im LMS* /f /t
taskkill /im jhi* /f /t
taskkill /im esif* /f /t
taskkill /im evteng* /f /t
taskkill /im regsrvc* /f /t
taskkill /im ibtsiva* /f /t
taskkill /im zeroconfig* /f /t
taskkill /im searchindexer* /f /t
taskkill /im acrotray* /f /t
taskkill /im adservice* /f /t
taskkill /im edge* /f /t
taskkill /im iexplore* /f /t
taskkill /im ccleaner* /f /t
taskkill /im richvideo* /f /t
sc stop NcdAutoSetup
sc stop HomeGroupProvider
sc stop netprofm
sc stop Nlasvc
sc stop NcaSvc
sc stop iphlpsvc
sc stop WinHttpAutoProxySvc
sc stop Dhcp
ECHO See above for status
Pause
